FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN apk update && \
    apk add --no-cache --virtual .build-deps \
    gcc \
    musl-dev \
    python3-dev \
    libffi-dev \
    openssl-dev \
    cargo \
    && apk add --no-cache postgresql-dev \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && apk del .build-deps

EXPOSE 5000

CMD /bin/sh -c "/usr/local/bin/python3 manage.py migrate && /usr/local/bin/python3 manage.py runserver 0.0.0.0:5000"