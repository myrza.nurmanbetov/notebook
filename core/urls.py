from django.urls import path
from .views import PostViews, read_post, profile, creat_post



urlpatterns = [
    path('posts/', PostViews.as_view(), name="posts"),
    path('post/<int:pk>/', read_post, name='post'),
    path('profile/<int:pk>/', profile, name='profile'),
    path('post_create/', creat_post, name='post_create'),    
    ]