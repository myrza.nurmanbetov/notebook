from .models import Post, Comment
from django.views.generic import ListView
from django.shortcuts import render
from django.contrib.auth.models import User
from .forms import PostCreateForm
from django.contrib import messages
from django.shortcuts import render, redirect




class PostViews(ListView):
    model = Post



def read_post(request, pk, template_name='core/post.html'):

    po = Post.objects.raw("SELECT * FROM core_post")
    post = Post.objects.get(pk=pk)
    comment = post.comment.all() 
    context = {'posts':post, 'comments':comment, 'obj':po}
    return render(request, template_name, context)  


def profile(request, pk,template_name='core/profile.html'):
    user = User.objects.get(pk = pk)
    context = {'user': user}
    return render(request, template_name, context)



def creat_post(request):
    if request.method == "POST":
        post_form = PostCreateForm(request.POST, request.FILES)
        print(post_form)
        if post_form.is_valid():
            post = post_form.save()  # Save the form data to create a new post
            return redirect("posts")
        
    else:
        post_form = PostCreateForm()
    return render(request=request, template_name='core/create_post.html', context={"post_form": post_form})

    