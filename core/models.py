from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Post(models.Model):
    name = models.CharField(max_length=150)
    body = models.TextField()
    owner = models.CharField(max_length=150, null=True, blank=True)
    public_date = models.DateTimeField(verbose_name="Date of publication", default=timezone.now)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return str(self.name)


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comment')
    author = models.CharField(max_length=150, null=True, blank=True)
    content = models.TextField()
    date_of_public = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.post)
    