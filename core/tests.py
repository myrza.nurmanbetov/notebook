from django.test import TestCase, Client
from django.core.exceptions import ValidationError
from .models import Post, User, Comment


class PostTest(TestCase):
    def setUp(self) -> None:
        self.post_model = Post.objects.create(name="New", body="Helloo", owner='owner')
        self.comment = Comment.objects.create(post=self.post_model, author='owner', content='Its new comment')

    def test_model_creation(self):
        self.assertEqual(self.post_model.name, "New")
        self.assertEqual(self.comment.content, 'Its new comment')


    def test_model_update(self):

        self.post_model.name = "Old Post"
        self.post_model.save()
        self.assertEqual(self.post_model.name, "Old Post")
        self.comment.content = "Its old comment now"
        self.comment.save()
        self.assertEqual(self.comment.content, "Its old comment now")

    def test_model_deletion(self):
        self.post_model.delete()
        self.comment.delete()
        self.assertFalse(Post.objects.exists())
        self.assertFalse(Comment.objects.exists())

    
class QueryingTestCase(TestCase):
    def setUp(self) -> None:    
        Post.objects.create(name="Post", body='Post', owner='owner')
        Post.objects.create(name="Python is Awesome", body='Post1', owner='owner')

    def test_get_all_obj(self):
        items = Post.objects.all()
        self.assertEqual(items.count(),2)

    def test_filter_obj(self):
        filtered = Post.objects.filter(name__contains='Post')
        self.assertEqual(filtered.count(), 1)
        self.assertEqual(filtered[0].name, 'Post')




